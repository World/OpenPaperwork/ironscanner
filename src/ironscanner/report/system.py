import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def get_interfaces(self):
        return ['scan_report']

    def get_deps(self):
        return [
            {
                'interface': 'stats',
                'defaults': ['paperwork_backend.beacon.sysinfo'],
            },
        ]

    def get_info_for_user(self, out: dict):
        stats = {}
        self.core.call_all("stats_get", stats)
        out['sys'] = stats

    def get_scan_report_dict(self, out: dict):
        if 'system' not in out:
            out['system'] = {}

        stats = {}
        self.core.call_all("stats_get", stats)
        out['system'].update({
            "sys_" + k: v
            for (k, v) in stats.items()
        })
        LOGGER.info("Report: system=%s", out['system'])
