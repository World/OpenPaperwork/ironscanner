# -*- mode: python -*-

import os
import sys

block_cipher = None

SINGLE_BINARY = True
BASE_PATH = os.getcwd()

# XXX(Jflesch): This is too annoying to maintain for all systems.
# Debian stable 64 bits is the reference here. Use a container if you have to.

if 'LD_LIBRARY_PATH' in os.environ:
    # We assume LD_LIBRARY_PATH is used only to point to a local installation
    # of Libinsane (--> not system-wide).
    # It won't be used here for anything else than Libinsane. For the other
    # dependencies, we assume they are installed system-wide.
    lib_path = os.path.join(
        os.environ['LD_LIBRARY_PATH'].split(os.path.pathsep)[0],
        'x86_64-linux-gnu'
    )
else:
    lib_path = "/usr/lib/x86_64-linux-gnu"
typelib_path = os.path.join(lib_path, 'girepository-1.0')


print("BASEPATH: {}".format(BASE_PATH))
print("lib_path: {}".format(lib_path))

datas = []
for (dirpath, subdirs, filenames) in os.walk(BASE_PATH):
    shortdirpath = dirpath[len(BASE_PATH):]
    if ("dist" in shortdirpath.lower()
            or "build" in shortdirpath.lower()
            or "egg" in shortdirpath.lower()
            or "tox" in shortdirpath.lower()):
        continue
    for filename in filenames:
        (_, ext) = os.path.splitext(filename)
        ext = ext.lower()
        if ext == ".png" and shortdirpath.lower().endswith("doc"):
            continue
        if (ext != ".ico"
                and ext !=".png"
                and ext != ".svg"
                and ext != ".xml"
                and ext != ".glade"
                and ext != ".css"
                and ext != ".mo"
                and ext != ".txt"
                and ext != ".pdf"):
            continue
        filepath = os.path.join(dirpath, filename)

        dest = "data"
        print(
            "=== Adding file [{}] --> [{}] ===".format(filepath, dest)
        )
        datas.append((filepath, dest))

bins = [
    (os.path.join(typelib_path, 'Libinsane-1.0.typelib'), 'gi_typelibs'),
    # .typelib can point to any of those .so, and I'm too lazy
    # to figure out which one exactly
    (os.path.join(lib_path, 'libinsane.so'), '.'),
    (os.path.join(lib_path, 'libinsane_gobject.so'), '.'),
    (os.path.join(lib_path, 'libinsane.so.1'), '.'),
    (os.path.join(lib_path, 'libinsane_gobject.so.1'), '.'),
    (os.path.join(lib_path, 'libinsane.so.1.0.9'), '.'),
    (os.path.join(lib_path, 'libinsane_gobject.so.1.0.9'), '.'),
    (
        "/usr/lib/x86_64-linux-gnu/gdk-pixbuf-2.0/2.10.0/loaders.cache",
        "lib/gdk-pixbuf-2.0/2.10.0"
    ),
]


a = Analysis(['launcher.py'],
             pathex=[],
             binaries=bins,
             datas=datas,
             hiddenimports=[
                "openpaperwork_core",
                "openpaperwork_gtk",
                "paperwork_backend",
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
          cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          exclude_binaries=not SINGLE_BINARY,
          name='ironscanner',
          debug=True,
          strip=False,
          upx=False,
          runtime_tmpdir=None,
          console=True)
if not SINGLE_BINARY:
    coll = COLLECT(
        exe,
        a.binaries,
        a.zipfiles,
        a.datas,
        strip=False,
        upx=False,
        name='ironscanner'
    )
