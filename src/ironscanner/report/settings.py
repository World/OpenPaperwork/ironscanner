import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.user_info = {}
        self.report_scantest_config = {}

    def get_interfaces(self):
        return ["scan_report"]

    def get_deps(self):
        return []

    def get_info_for_user(self, out: dict):
        out.update(self.user_info)

    def on_scanner_selected(self, *args, **kwargs):
        self.report_scantest_config['source'] = ""
        self.user_info['dev_source'] = ""
        self.report_scantest_config['mode'] = ""
        self.user_info['dev_mode'] = ""
        self.report_scantest_config['resolution'] = 0
        self.user_info['dev_resolution'] = 0

    def on_source_selected(self, source):
        self.report_scantest_config['source'] = source.source.get_name()
        self.user_info['dev_source'] = source.source.get_name().title()

    def on_scan_resolution_selected(self, resolution):
        self.report_scantest_config['resolution'] = resolution
        self.user_info['dev_resolution'] = resolution

    def on_scan_mode_selected(self, mode):
        self.report_scantest_config['mode'] = mode
        self.user_info['dev_mode'] = mode

    def get_scan_report_dict(self, out: dict):
        if 'scantest' not in out:
            out['scantest'] = {}
        LOGGER.info(
            "Report: scan test config = %s", self.report_scantest_config
        )
        out['scantest']['config'] = self.report_scantest_config
