#!/usr/bin/env python3

import os
import setuptools
import sys
import cx_Freeze

executables = []
common_include_files = []
common_packages = []

executables.append(
    cx_Freeze.Executable(
        script="pyinstaller/launcher.py",
        target_name="ironscanner.exe",
        base=(
            "Console"
            if os.name != "nt" or os.getenv("DEBUG", False)
            else "Win32GUI"
        ),
    ),
)

required_dll_search_paths = os.getenv("PATH", os.defpath).split(os.pathsep)
required_dlls = [
    'libatk-1.0-0.dll',
    'libepoxy-0.dll',
    'libgdk-3-0.dll',
    'libgdk_pixbuf-2.0-0.dll',
    'libgtk-3-0.dll',
    'libhandy-1-0.dll',
    'libpango-1.0-0.dll',
    'libpangocairo-1.0-0.dll',
    'libpangoft2-1.0-0.dll',
    'libpangowin32-1.0-0.dll',
    'librsvg-2-2.dll',
    'libxml2-2.dll',

    'libinsane.dll',
    'libinsane_gobject.dll',
]

for dll in required_dlls:
    dll_path = None
    for p in required_dll_search_paths:
        p = os.path.join(p, dll)
        if os.path.isfile(p):
            dll_path = p
            break
    if dll_path is None:
        raise Exception(
            "Unable to locate {} in {}".format(
                dll, required_dll_search_paths
            )
        )
    common_include_files.append((dll_path, dll))

# We need the .typelib files at runtime.
# The related .gir files are in $PREFIX/share/gir-1.0/$NS.gir,
# but those can be omitted at runtime.

required_gi_namespaces = [
    "Atk-1.0",
    "cairo-1.0",
    "Gdk-3.0",
    "GdkPixbuf-2.0",
    "Gio-2.0",
    "GLib-2.0",
    "GModule-2.0",
    "GObject-2.0",
    "Gtk-3.0",
    "Handy-1",
    "HarfBuzz-0.0",
    "Pango-1.0",
    "PangoCairo-1.0",

    "Libinsane-1.0",
]

for ns in required_gi_namespaces:
    subpath = "lib/girepository-1.0/{}.typelib".format(ns)
    fullpath = os.path.join(sys.prefix, subpath)
    assert os.path.isfile(fullpath), (
        "Required file {} is missing" .format(
            fullpath,
        ))
    common_include_files.append((fullpath, subpath))

common_packages = [
    "setuptools",

    "openpaperwork_core",
    "openpaperwork_gtk",
    "paperwork_backend",
    "ironscanner",

    "six",
    "gi",   # always seems to be needed
    "cairo",   # Only needed (for foreign structs) if no "import cairo"s
]


cx_Freeze.setup(
    name="ironscanner",
    version="2.0",
    description=(
        "Scanner information collector"
    ),
    long_description=(
        "Collect as much information as possible regarding"
        " image scanners."
    ),
    keywords="scanner gui",
    url="https://gitlab.gnome.org/World/OpenPaperwork/ironscanner",
    download_url=(
        "https://gitlab.gnome.org/World/OpenPaperwork/ironscanner/-/archive/"
        "1.1.0/ironscanner-2.0.tar.gz"
    ),
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: X11 Applications :: GTK",
        "Environment :: X11 Applications :: Gnome",
        "Intended Audience :: End Users/Desktop",
        ("License :: OSI Approved ::"
         " GNU General Public License v3 or later (GPLv3+)"),
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Topic :: Multimedia :: Graphics :: Capture :: Scanners",
    ],
    license="GPLv3+",
    author="Jerome Flesch",
    author_email="jflesch@openpaper.work",
    packages=setuptools.find_packages('src'),
    package_dir={
        '': 'src',
        'ironscanner': 'src/ironscanner',
    },
    include_package_data=True,
    entry_points={
        'gui_scripts': [
            'ironscanner = ironscanner.main:main',
        ]
    },
    zip_safe=False,
    install_requires=[
        "openpaperwork-core",
        "openpaperwork-gtk",
        "Pillow",
        "psutil",
    ],
    setup_requires=[
        "setuptools_scm",
    ],
    executables=executables,
    options={
        "build_exe": {
            'include_files': common_include_files,
            'silent': True,
            'packages': common_packages,
            "excludes": ["tkinter", "tk", "tcl"]
        },
    },
)
