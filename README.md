# IronScanner

![Logo](src/ironscanner/gui/mainform/logo.png)

Collect as much information as possible on a image scanner, run test scan, and send a detailed report to [OpenPaper.work](https://openpaper.work/scanner_db/).


## Windows

[Download](https://download.openpaper.work/windows/x86/ironscanner-master-latest.exe), click "yes" on the ten of thousands of security warnings, and run.


## Linux

[Download](https://download.openpaper.work/linux/amd64/ironscanner-master-latest.appimage),
make executable, and run.


## Linux, but from source code

```sh
# Build dependencies for Libinsane
sudo apt install \
        python3 python3-virtualenv virtualenv python3-setuptools \
        python3-dev python3-gi \
        libgirepository1.0-dev gobject-introspection \
        python3-pil \
        make meson \
        build-essential \
        libsane-dev \
        doxygen \
        libcunit1-ncurses-dev \
        valac

cd /tmp
git clone https://gitlab.gnome.org/World/OpenPaperwork/ironscanner.git

cd ironscanner
source ./activate_test_env.sh
make install  # will run python3 ./setup.py install

# Install any other missing dependencies (GTK, etc)
ironscanner chkdeps

# And run !
ironscanner
```


## Contact

- [Forum](https://forum.openpaper.work/)
- [Bug tracker](https://gitlab.gnome.org/World/OpenPaperwork/ironscanner/issues)
