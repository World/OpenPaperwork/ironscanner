import logging

import PIL

try:
    import gi
    gi.require_version('Libinsane', '1.0')
    from gi.repository import Libinsane
    LIBINSANE_AVAILABLE = True
except (ImportError, ValueError):
    LIBINSANE_AVAILABLE = False

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def get_interfaces(self):
        return [
            "chkdeps",
            "scan_report"
        ]

    def get_deps(self):
        return []

    def chkdeps(self, out: dict):
        if not LIBINSANE_AVAILABLE:
            out['libinsane'] = {
                'debian': 'gir1.2-libinsane-1.0',
                'linuxmint': 'gir1.2-libinsane-1.0',
                'raspbian': 'gir1.2-libinsane-1.0',
                'ubuntu': 'gir1.2-libinsane-1.0',
            }

    def get_scan_report_dict(self, out: dict):
        if 'system' not in out:
            out['system'] = {}
        out['system']['versions'] = {
            'pillow': (
                "unknown"
                if not hasattr(PIL, "__version__")
                else PIL.__version__
            ),
            'scan_library': "Libinsane " + Libinsane.Api.get_version(),
            'test_program': "{} {}".format(
                self.core.call_success("app_get_name"),
                self.core.call_success("app_get_version"),
            )
        }
        LOGGER.info("Report: versions=%s", out['system']['versions'])
