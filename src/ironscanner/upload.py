import io
import locale
import logging
import os
import platform

import requests

import openpaperwork_core
import openpaperwork_core.promise

from . import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    API_KEY_HEADER = {
        'Authorization': os.getenv(
            "IRONSCANNER_API_KEY",
            "Api-Key QKjkpxPI.Pgz40S7ACTtlpJiqnw81mCT3VxCxTvtO"
        ),
    }

    def __init__(self):
        self.base_url = os.getenv(
            "IRONSCANNER_UPLOAD_API_URI",
            "https://openpaper.work/api/v1/scannerdb/"
        )

    def get_interfaces(self):
        return ['scan_report_upload']

    def get_deps(self):
        return [
            {
                'interface': 'scan_report',
                'defaults': []
            },
        ]

    def _build_report(self):
        report = {}
        report['json'] = {}
        report['attachments'] = []

        self.core.call_all("get_scan_report_dict", report['json'])
        self.core.call_all(
            "get_scan_report_attachments", report['attachments']
        )

        return report

    @staticmethod
    def _check_status_code_200(r):
        if r.status_code - (r.status_code % 100) != 200:
            raise Exception("Unexpected HTTP reply: {} - {}\n{}".format(
                r.status_code, r.reason, r.text
            ))

    @staticmethod
    def _get_json(r):
        try:
            return r.json()
        except Exception:
            raise Exception("Got invalid JSON: {}".format(r.text))

    def _get_manufacturer_url(self, routes, manufacturer):
        manufacturer = manufacturer.strip().lower()

        self.core.call_one(
            "mainloop_schedule", self.core.call_all,
            "on_report_upload_progress", 1,
            _("Searching scanner manufacturer '%s' ...") % manufacturer
        )

        url = routes['scanner_manufacturers']
        while True:
            LOGGER.info(
                "Looking for manufacturer '%s' at '%s' ...",
                manufacturer, url
            )
            r = requests.get(url, self.API_KEY_HEADER)
            self._check_status_code_200(r)
            r = self._get_json(r)

            for result in r['results']:
                if result['name'].strip().lower() == manufacturer:
                    return result['url']

            if 'next' not in r or r['next'] is None:
                return None

            assert url != r['next']
            url = r['next']

    def _create_manufacturer(self, routes, manufacturer):
        manufacturer = manufacturer.strip().lower()
        LOGGER.info(
            "Creating manufacturer '%s' at '%s' ...",
            manufacturer, routes['scanner_manufacturers']
        )
        r = requests.post(
            routes['scanner_manufacturers'], headers=self.API_KEY_HEADER,
            data={"name": manufacturer},
            allow_redirects=False
        )
        self._check_status_code_200(r)
        return self._get_json(r)['url']

    def _get_model_url(self, model, manufacturer_url):
        model = model.strip().lower()

        self.core.call_one(
            "mainloop_schedule", self.core.call_all,
            "on_report_upload_progress", 1,
            _("Searching scanner model '%s' ...") % model
        )
        LOGGER.info("Getting scanner list from %s ...", manufacturer_url)
        r = requests.get(manufacturer_url, headers=self.API_KEY_HEADER)
        self._check_status_code_200(r)
        r = self._get_json(r)
        for scanner_url in r['scanners']:
            LOGGER.info("Examining scanner at %s ...", scanner_url)
            r = requests.get(scanner_url, headers=self.API_KEY_HEADER)
            self._check_status_code_200(r)
            r = self._get_json(r)
            if r['name'].strip().lower() == model:
                return scanner_url
        return None

    def _create_model(self, routes, model, manufacturer_url):
        model = model.strip().lower()
        LOGGER.info(
            "Creating scanner '%s' at '%s' ...", model, routes['scanners']
        )
        r = requests.post(
            routes['scanners'], headers=self.API_KEY_HEADER,
            data={
                "manufacturer": manufacturer_url,
                "name": model,
            },
            allow_redirects=False
        )
        self._check_status_code_200(r)
        r = self._get_json(r)
        if 'url' not in r:
            raise Exception(
                "Failed to create scanner model on remote: {}".format(r)
            )
        return r['url']

    def _send_report(self, report):
        """
        Beware, it runs in a dedicated thread
        """
        r_json = report['json']

        self.core.call_one(
            "mainloop_schedule", self.core.call_all,
            "on_report_upload_progress", 1, _("Contacting openpaper.work ...")
        )
        LOGGER.info("Accessing %s ...", self.base_url)
        r = requests.get(self.base_url)
        self._check_status_code_200(r)
        routes = self._get_json(r)

        manufacturer_url = self._get_manufacturer_url(
            routes, r_json['scanner']['vendor']
        )
        if manufacturer_url is None:
            manufacturer_url = self._create_manufacturer(
                routes, r_json['scanner']['vendor']
            )

        model_url = self._get_model_url(
            r_json['scanner']['model'], manufacturer_url
        )
        if model_url is None:
            model_url = self._create_model(
                routes, r_json['scanner']['model'], manufacturer_url
            )

        self.core.call_one(
            "mainloop_schedule", self.core.call_all,
            "on_report_upload_progress", 10, _("Uploading report ...")
        )
        # TODO(Jflesch): this implies dependency on the values provided
        # by various plugins --> not clean
        lang = locale.getdefaultlocale()[0]
        LOGGER.info("System language: %s", lang)
        data = {
            "scanner": model_url,
            "sealed": False,
            "successful": r_json['scantest']['successful'],
            "os": platform.system(),
            "scan_library": r_json['system']['versions']['scan_library'],
            "scan_program": r_json['system']['versions']['test_program'],
            "scan_source": r_json['scantest']['config']['source'],
            "scanner_type": (
                r_json['scanner']['type']
                if r_json['scanner']['type'] != ""
                else "unknown"
            ),
            "user_email": self.core.call_success("get_user_email"),
            "locale": lang,
        }
        LOGGER.info("Creating report at %s ...", routes['scan_reports'])
        r = requests.post(
            routes['scan_reports'], headers=self.API_KEY_HEADER, data=data,
            allow_redirects=False
        )
        self._check_status_code_200(r)
        r = self._get_json(r)

        report_secret = r['secret_key']
        # TODO(Jflesch): We need the user URL, not the API url
        report_url = r['url']
        report_user_url = r['user_url']
        LOGGER.info("Report URL: %s", report_url)

        self.core.call_one(
            "mainloop_schedule", self.core.call_all,
            "on_report_upload_progress", 10,
            _("Uploading report (JSON) ...")
        )
        data = {
            'secret_key': report_secret,
            'data': r_json,
        }
        LOGGER.info("Uploading JSON at '%s' ...", report_url + "data")
        r = requests.post(
            report_url + "data", headers=self.API_KEY_HEADER, json=data,
            allow_redirects=False
        )
        self._check_status_code_200(r)

        progress = 20
        progress_step = (90 - 20) / len(report['attachments'])
        for (attachment_name, attachment_url) in report['attachments']:
            self.core.call_one(
                "mainloop_schedule", self.core.call_all,
                "on_report_upload_progress", progress,
                _("Uploading attachment: %s ..." % (attachment_name,))
            )
            content = io.BytesIO()
            with self.core.call_success("fs_open", attachment_url, "rb") as fd:
                r = fd.read()
                content_size = len(r)
                content.write(r)
            content.seek(0)

            data = {
                "report": report_url,
                "file_name": attachment_name,
                "secret_key": report_secret,
            }
            files = {
                "attachment": content,
            }
            LOGGER.info(
                "Uploading attachment '%s' (%dB) at '%s' ...",
                attachment_name, content_size,
                routes['scan_report_attachments']
            )
            r = requests.post(
                routes['scan_report_attachments'],
                headers=self.API_KEY_HEADER,
                data=data,
                files=files,
                allow_redirects=False
            )
            self._check_status_code_200(r)
            progress += progress_step

        self.core.call_one(
            "mainloop_schedule", self.core.call_all,
            "on_report_upload_progress", progress,
            _("Sealing the report ...")
        )

        data = {
            "scanner": model_url,
            "sealed": False,
            "successful": r_json['scantest']['successful'],
            "os": platform.system(),
            "scan_library": r_json['system']['versions']['scan_library'],
            "scan_program": r_json['system']['versions']['test_program'],
            "scan_source": r_json['scantest']['config']['source'],
            "scanner_type": (
                r_json['scanner']['type']
                if r_json['scanner']['type'] != ""
                else "unknown"
            ),
            "user_email": self.core.call_success("get_user_email"),
            "secret_key": report_secret,
        }
        LOGGER.info("Sealing report at '%s' ...", report_url)
        r = requests.put(
            report_url, headers=self.API_KEY_HEADER, data=data
        )
        self._check_status_code_200(r)

        return report_user_url

    def upload_scan_report(self):
        promise = openpaperwork_core.promise.Promise(
            self.core, self._build_report
        )
        promise = promise.then(openpaperwork_core.promise.ThreadedPromise(
            self.core, self._send_report
        ))

        def on_success(url):
            self.core.call_all("on_report_upload_complete", True, url)

        def on_error(exc):
            self.core.call_all("on_report_upload_complete", False, None)
            raise exc

        promise = promise.then(on_success)
        promise.catch(on_error)
        promise.schedule()
