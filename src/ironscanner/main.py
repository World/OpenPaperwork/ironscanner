import argparse
import logging
import os
import sys

import openpaperwork_core
import openpaperwork_gtk

from . import _


LOGGER = logging.getLogger(__name__)


DEFAULT_PLUGINS = (
    openpaperwork_core.RECOMMENDED_PLUGINS +
    openpaperwork_gtk.CLI_PLUGINS +
    openpaperwork_gtk.GUI_PLUGINS +
    [
        'ironscanner.app',
        'ironscanner.gui.mainform',
        'ironscanner.gui.pages.intro',
        'ironscanner.gui.pages.scan',
        'ironscanner.gui.pages.scan_result',
        'ironscanner.gui.pages.settings',
        'ironscanner.gui.pages.summary_after',
        'ironscanner.gui.pages.summary_before',
        'ironscanner.gui.pages.upload',
        'ironscanner.gui.pages.upload_result',
        'ironscanner.report.device',
        'ironscanner.report.images',
        'ironscanner.report.logs',
        'ironscanner.report.settings',
        'ironscanner.report.success',
        'ironscanner.report.system',
        'ironscanner.report.versions',
        'ironscanner.report.user',
        'ironscanner.upload',
        'openpaperwork_core.archives',
        'openpaperwork_core.beacon.sysinfo',
        'openpaperwork_core.bug_report.censor',
        'openpaperwork_core.censor',
        'openpaperwork_core.cmd.chkdeps',
        'openpaperwork_core.frozen',
        'openpaperwork_core.http',
        'openpaperwork_core.logs.archives',
        'openpaperwork_core.paths.xdg',
        'openpaperwork_core.pillow.img',
        'openpaperwork_core.data_versioning',
        'openpaperwork_gtk.drawer.pillow',
        'openpaperwork_gtk.drawer.scan',
        'paperwork_backend.cairo.pillow',
        'paperwork_backend.docscan.libinsane',
    ]
)


def main():
    disabled_plugins = os.environ.get("IRONSCANNER_DISABLED_PLUGINS", "")
    disabled_plugins = disabled_plugins.split(":")

    core = openpaperwork_core.Core()
    core.load("openpaperwork_core.config.fake")  # no config file here
    core.load("openpaperwork_core.fs.python")
    core.load("openpaperwork_core.logs.print")
    core.load("openpaperwork_core.uncaught_exception")
    core.load("openpaperwork_gtk.fs.gio")
    core.load("openpaperwork_gtk.mainloop.glib")
    core.init()
    core.call_all("init_logs", "ironscanner", "info")

    for module_name in DEFAULT_PLUGINS:
        if module_name in disabled_plugins:
            continue
        core.load(module_name)
    core.init()

    args = sys.argv[1:]
    if len(args) <= 0:
        core.call_all("on_initialized")
        LOGGER.info("Ready")
        core.call_one("mainloop", halt_on_uncaught_exception=False)
        LOGGER.info("Quitting")
        core.call_all("on_quit")
    else:
        parser = argparse.ArgumentParser()
        cmd_parser = parser.add_subparsers(
            help=_('command'), dest='command', required=True
        )

        core.call_all("cmd_complete_argparse", cmd_parser)
        args = parser.parse_args(args)

        core.call_all("cmd_set_interactive", True)

        core.call_all("cmd_run", args)
        core.call_all("on_quit")


if __name__ == "__main__":
    main()
