import logging

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps

from .... import _


LOGGER = logging.getLogger(__name__)


TEMPLATE = (
    "Summary of the test:\n"
    "- Scanner: {dev_name}\n"
    "- Type: {dev_type}\n"
    "- Source: {dev_source}\n"
    "- Resolutions: {dev_resolution}\n"
    "- Mode: {dev_mode}\n"
    "\n"
    "Personal information that will be attached to the report:\n"
    "- Name: {user_name}\n"
    "- Email: xxxxxx\n"
    "\n"
    "System informations that will be attached to the report:\n"
    "{sys}\n"
)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 250

    def __init__(self):
        super().__init__()
        self.widget_tree = None
        self.info = {}

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_assistant_page',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_assistant',
                'defaults': ['ironscanner.gui.mainform'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def init(self, core):
        super().init(core)

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "ironscanner.gui.pages.summary_before", "summary_before.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

    def on_initialized(self):
        self.core.call_all(
            "assistant_add_page",
            Gtk.AssistantPageType.CONFIRM,
            _("Ready ?"),
            self.widget_tree.get_object("page_summary_before")
        )

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_assistant_prepare(self, page):
        if page is not self.widget_tree.get_object("page_summary_before"):
            return
        self.core.call_all("assistant_set_page_complete", page)

        info = {}
        self.core.call_all("get_info_for_user", info)
        LOGGER.info("Info: %s", str(info))
        info['sys'] = "\n".join([
            "- {}: {}".format(k, v) for (k, v) in info['sys'].items()
        ])
        summary = TEMPLATE.format(**info)
        self.widget_tree.get_object("textbufferSummaryBefore").set_text(
            summary
        )
