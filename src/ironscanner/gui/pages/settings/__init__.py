import logging

try:
    import gi
    gi.require_version('GdkPixbuf', '2.0')
    gi.require_version('Gtk', '3.0')
    from gi.repository import (
        GdkPixbuf,
        Gtk
    )
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps

from .... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 500

    def __init__(self):
        super().__init__()
        self.widget_tree = None
        self.page_active = False
        self.scanner = None

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_assistant_page',
            'scanner_settings_input',
            'user_info_input',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'config',
                'defaults': ['openpaperwork_core.config'],
            },
            {
                'interface': 'gtk_assistant',
                'defaults': ['ironscanner.gui.mainform'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'scan',
                'defaults': ['openpaperwork_core.docscan.libinsane'],
            },
        ]

    def init(self, core):
        super().init(core)

        if not GTK_AVAILABLE:
            return

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "ironscanner.gui.pages.settings", "settings.glade"
        )

        self.widget_tree.get_object("checkboxAcceptContact").connect(
            "toggled", self._on_accept_contact_toggled
        )
        self._on_accept_contact_toggled()

        self.widget_tree.get_object("comboboxDevices").connect(
            "changed", self._on_scanner_selected
        )
        self.widget_tree.get_object("comboboxSources").connect(
            "changed", self._on_source_selected
        )

        self.widget_tree.get_object("comboboxScannerTypes").connect(
            "changed", self._on_type_selected
        )

        self.widget_tree.get_object("comboboxResolutions").connect(
            "changed", self._on_resolution_selected
        )
        self.widget_tree.get_object("comboboxModes").connect(
            "changed", self._on_mode_selected
        )

    @staticmethod
    def _fix_scanner_name(name):
        name = name.lower()
        name = name.replace("_", " ")
        name = name.replace("/", " ")
        # this is marketing crap that is found mostly on HP scanners but on
        # some other too
        name = name.replace(" professional", "")
        name = name.replace(" pro", "")
        name = name.replace(" series", "")
        name = name.replace(" photo", "")
        name = name.replace("pixma", "")  # Canon
        name = name.replace(" (usb)", "")  # Canon (Sane)
        name = name.strip()
        return name

    def _on_accept_contact_toggled(self, *args, **kwargs):
        checkbox = self.widget_tree.get_object("checkboxAcceptContact")
        active = checkbox.get_active()
        self.widget_tree.get_object("entryUserName").set_sensitive(active)
        self.widget_tree.get_object("entryUserEmail").set_sensitive(active)

    def _on_scanner_list(self, scanners):
        scanner_list = self.widget_tree.get_object("liststoreDevices")
        scanner_list.clear()
        if len(scanners) >= 0:
            scanner_list.append((_("Please select a scanner"), "", "", ""))
            for (dev_id, dev_name, dev_vendor, dev_model) in scanners:
                LOGGER.info("Scanner: %s - %s", dev_id, dev_name)
                dev_model = self._fix_scanner_name(dev_model)
                scanner_list.append((dev_name, dev_id, dev_vendor, dev_model))
        scanner_list.append(
            (_("Scanner not in the list"), "", "", "")
        )
        scanner_combobox = self.widget_tree.get_object("comboboxDevices")
        scanner_combobox.set_model(scanner_list)
        scanner_combobox.set_active(0)
        scanner_combobox.set_sensitive(True)

    def _on_scanner_selected(self, *args, **kwargs):
        if self.scanner is not None:
            self.scanner.close()
        scanner_devid = self.widget_tree.get_object("comboboxDevices")
        active = scanner_devid.get_active()
        if active < 0:
            return

        scanner = scanner_devid.get_model()[active]
        scanner_devid = scanner[1]
        LOGGER.info("Selected scanner: %s", scanner_devid)

        comboboxes = [
            self.widget_tree.get_object("comboboxResolutions"),
            self.widget_tree.get_object("comboboxModes"),
            self.widget_tree.get_object("comboboxSources"),
        ]

        if scanner_devid == "":  # Scanner not in the list
            self.widget_tree.get_object("entryManufacturer").set_text("")
            self.widget_tree.get_object("entryModel").set_text("")
            self.widget_tree.get_object("expanderScannerInfo").set_expanded(
                True
            )

            empty_list = self.widget_tree.get_object("liststoreLoading")
            for combobox in comboboxes:
                combobox.set_model(empty_list)
                combobox.set_sensitive(False)

        else:
            self.widget_tree.get_object("entryManufacturer").set_text(
                scanner[2]
            )
            self.widget_tree.get_object("entryModel").set_text(scanner[3])

            loading_list = self.widget_tree.get_object("liststoreLoading")
            for combobox in comboboxes:
                combobox.set_model(loading_list)
                combobox.set_active(0)
                combobox.set_sensitive(False)

            promise = self.core.call_success(
                "scan_get_scanner_promise", scanner_devid
            )
            promise = promise.then(self._on_scanner_obtained)
            promise = promise.catch(self._on_scanner_failure, scanner_devid)
            self.core.call_success("scan_schedule", promise)

        self.core.call_all("on_scanner_selected", *scanner)

        page = self.widget_tree.get_object("page_settings")
        self.core.call_all(
            "assistant_set_page_complete",
            page, scanner_devid == ""
        )

    def _on_scanner_obtained(self, scanner):
        self.scanner = scanner
        sources = self.widget_tree.get_object("liststoreSources")
        sources.clear()
        source_names = []
        for (source_name, source) in scanner.get_sources().items():
            LOGGER.info("Source: %s", source_name)
            sources.append((source_name.title(), source))
            source_names.append(source_name)
        combobox = self.widget_tree.get_object("comboboxSources")
        combobox.set_model(sources)
        combobox.set_active(0)
        combobox.set_sensitive(True)

        self.core.call_all("on_source_list", source_names)

        page = self.widget_tree.get_object("page_settings")
        self.core.call_all("assistant_set_page_complete", page)

    def _on_scanner_failure(self, exc, devid):
        LOGGER.error("Failed to access scanner '%s'", devid)
        failure_list = self.widget_tree.get_object("liststoreFailure")
        comboboxes = [
            self.widget_tree.get_object("comboboxResolutions"),
            self.widget_tree.get_object("comboboxModes"),
            self.widget_tree.get_object("comboboxSources"),
        ]
        for combobox in comboboxes:
            combobox.set_model(failure_list)
            combobox.set_active(0)
            combobox.set_sensitive(False)

        raise exc

    def _on_source_selected(self, *args, **kwargs):
        sources = self.widget_tree.get_object("comboboxSources")
        (human, source) = self.__get_combobox_value(sources)

        if isinstance(source, int):
            # We are on one of the default liststore
            return

        comboboxes = [
            (
                "resolutions",
                self.widget_tree.get_object("comboboxResolutions"),
                self.widget_tree.get_object("liststoreResolutions"),
                source.get_resolutions(),
                300,
            ),
            (
                "modes",
                self.widget_tree.get_object("comboboxModes"),
                self.widget_tree.get_object("liststoreModes"),
                source.get_modes(),
                "Color"
            ),
        ]
        for (name, widget, model, values, recommended) in comboboxes:
            model.clear()
            selected = 0
            for (idx, value) in enumerate(values):
                LOGGER.info("Option '%s': possible value: %s", name, value)
                if str(value).lower() == str(recommended).lower():
                    model.append((
                        str(value).title() + " " + _("(recommended)"),
                        value
                    ))
                    selected = idx
                else:
                    model.append((str(value).title(), value))
            widget.set_model(model)
            widget.set_active(selected)
            widget.set_sensitive(True)

        options = {}
        for opt in source.source.get_options():
            options[opt.get_name()] = opt

        self.core.call_all("on_source_selected", source)
        self.core.call_all("on_source_options", options)

    def _on_type_selected(self, *args, **kwargs):
        types = self.widget_tree.get_object("comboboxScannerTypes")
        if types.get_active() < 0:
            img = None
            scanner_type = None
        else:
            scanner_type = types.get_model()[types.get_active()]
            img = scanner_type[2]
            scanner_type = scanner_type[1]

        img_widget = self.widget_tree.get_object("imageScanner")

        if img is None or img == "":
            img_widget.set_from_icon_name(
                "gtk-missing-image", Gtk.IconSize.DIALOG
            )
            self.core.call_all("on_scanner_type_selected", "")
        else:
            LOGGER.info("Scanner type image: %s", img)
            pixbuf = self.core.call_success(
                "gtk_load_pixbuf", "ironscanner.gui.pages.settings", img
            )
            (pw, ph) = (pixbuf.get_width(), pixbuf.get_height())
            rect = img_widget.get_allocation()
            ratio = max(pw / rect.width, pw / rect.height)
            pixbuf = pixbuf.scale_simple(
                int(pw / ratio), int(ph / ratio),
                GdkPixbuf.InterpType.BILINEAR
            )
            img_widget.set_from_pixbuf(pixbuf)
            self.core.call_all("on_scanner_type_selected", scanner_type)

    def __get_combobox_value(self, combobox):
        model = combobox.get_model()
        active = combobox.get_active()
        if active < 0:
            return (None, -1)
        return model[active]

    def _on_resolution_selected(self, *args, **kwargs):
        (human, real) = self.__get_combobox_value(
            self.widget_tree.get_object("comboboxResolutions")
        )
        if real <= 0:
            return
        self.core.call_all("on_scan_resolution_selected", real)
        LOGGER.info("Resolution = %d", real)
        self.core.call_all("config_put", "scanner_resolution", real)

    def _on_mode_selected(self, *args, **kwargs):
        (human, real) = self.__get_combobox_value(
            self.widget_tree.get_object("comboboxModes")
        )
        if isinstance(real, int):
            return
        self.core.call_all("on_scan_mode_selected", real)
        LOGGER.info("Mode = %s", real)
        self.core.call_all("config_put", "scanner_mode", real)

    def on_initialized(self):
        page = self.widget_tree.get_object("page_settings")
        self.core.call_all(
            "assistant_add_page",
            Gtk.AssistantPageType.CONTENT,
            _("Test parameters"),
            page
        )

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_assistant_prepare(self, page):
        if page is not self.widget_tree.get_object("page_settings"):
            if self.page_active:
                self.page_active = False
                self._broadcast_user_info()
                self._broadcast_device_info()
            return

        self.page_active = True
        promise = self.core.call_success("scan_list_scanners_promise")
        promise = promise.then(self._on_scanner_list)
        self.core.call_success("scan_schedule", promise)

        self.core.call_all("on_scanner_type_selected", "unknown")

    def _broadcast_user_info(self):
        checkbox = self.widget_tree.get_object("checkboxAcceptContact")
        active = checkbox.get_active()
        if active:
            self.core.call_all(
                "on_user_name",
                self.widget_tree.get_object("entryUserName").get_text()
            )
            self.core.call_all(
                "on_user_email",
                self.widget_tree.get_object("entryUserEmail").get_text()
            )
        else:
            self.core.call_all("on_user_name", "")
            self.core.call_all("on_user_email", "")

    def _broadcast_device_info(self):
        manufacturer = self.widget_tree.get_object("entryManufacturer")
        manufacturer = manufacturer.get_text()
        model = self.widget_tree.get_object("entryModel")
        model = model.get_text()
        self.core.call_all("on_scanner_info", manufacturer, model)
