import logging

try:
    from gi.repository import Gio
    from gi.repository import GLib
    GLIB_AVAILABLE = True
except (ImportError, ValueError):
    GLIB_AVAILABLE = False

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -10000

    def __init__(self):
        super().__init__()
        self.widget_tree = None
        self.stack_box = {}

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_assistant',
            'gtk_mainwindow'
        ]

    def get_deps(self):
        return [
            {
                'interface': 'fs',
                'defaults': ['openpaperwork_gtk.fs.gio'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def init(self, core):
        super().init(core)

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "ironscanner.gui.mainform", "mainform.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

        mainwindow = self.widget_tree.get_object("mainform")
        mainwindow.connect("cancel", self._on_destroy)
        mainwindow.connect("close", self._on_destroy)
        mainwindow.connect("destroy", self._on_destroy)
        mainwindow.connect("escape", self._on_destroy)
        mainwindow.connect("prepare", self._on_assistant_prepare)

        if hasattr(GLib, 'set_application_name'):
            GLib.set_application_name("IronScanner")
        GLib.set_prgname("work.openpaper.Ironscanner")

        app = Gtk.Application(
            application_id=None,
            flags=Gio.ApplicationFlags.FLAGS_NONE
        )
        app.register(None)
        Gtk.Application.set_default(app)
        mainwindow.set_application(app)

    def on_initialized(self):
        mainwindow = self.widget_tree.get_object("mainform")
        mainwindow.set_visible(True)
        self.core.call_all("on_gtk_window_opened", mainwindow)

    def chkdeps(self, out: dict):
        if not GLIB_AVAILABLE:
            out['glib'].update(openpaperwork_gtk.deps.GLIB)
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_quit(self):
        if self.widget_tree is None:
            return
        mainwindow = self.widget_tree.get_object("mainform")
        mainwindow.set_visible(False)
        self.widget_tree = None

    def _on_destroy(self, mainwindow):
        LOGGER.info("Main window destroy. Quitting")
        self.core.call_all("on_gtk_window_closed", mainwindow)
        self.core.call_all("on_quit")
        self.core.call_all("mainloop_quit_graceful")

    def assistant_add_page(self, page_type, name, page):
        LOGGER.info("Adding page '%s'", name)
        mainwindow = self.widget_tree.get_object("mainform")
        mainwindow.append_page(page)
        mainwindow.set_page_title(page, name)
        mainwindow.set_page_type(page, page_type)

    def assistant_set_page_complete(self, page, complete=True):
        mainwindow = self.widget_tree.get_object("mainform")
        mainwindow.set_page_complete(page, complete)

    def assistant_commit(self):
        mainwindow = self.widget_tree.get_object("mainform")
        mainwindow.commit()

    def _on_assistant_prepare(self, assistant, page):
        self.core.call_all("on_assistant_prepare", page)
