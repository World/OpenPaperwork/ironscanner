VERSION_FILE = src/ironscanner/_version.py
PYTHON ?= python3

build: build_py

build_c:

build_py: ${VERSION_FILE}
	echo "Building IronScanner"
	${PYTHON} ./setup.py build

${VERSION_FILE}:
	echo -n "version = \"" >| $@
	echo -n $(shell git describe --always) >> $@
	echo "\"" >> $@

version: ${VERSION_FILE}

clean:
	rm -rf AppDir appimage-build
	rm -rf venv
	rm -rf build dist
	rm -rf src/ironscanner.egg-info
	rm -f src/ironscanner/version.txt
	[ -d sub/libinsane ] && ${MAKE} -C sub/libinsane clean || true
	[ -d sub/paperwork/openpaperwork-core ] && $(MAKE) -C sub/paperwork/openpaperwork-core clean
	[ -d sub/paperwork/openpaperwork-gtk ] && $(MAKE) -C sub/paperwork/openpaperwork-gtk clean
	[ -d sub/paperwork/paperwork-backend ] && $(MAKE) -C sub/paperwork/paperwork-backend clean

dist-clean: clean
	rm -rf sub

doc:

check:
	flake8 *.py src

test:

install: install_py

install_c:

install_py: version
	echo "Installing IronScanner"
	$(MAKE) -C sub/paperwork/openpaperwork-core install_py
	$(MAKE) -C sub/paperwork/openpaperwork-gtk install_py
	$(MAKE) -C sub/paperwork/paperwork-backend install_py
	${PYTHON} -m pip install ${PIP_ARGS} .

uninstall:
	echo "Uninstalling IronScanner"
	pip3 uninstall -y ironscanner
	$(MAKE) -C sub/paperwork/openpaperwork-core uninstall
	$(MAKE) -C sub/paperwork/openpaperwork-gtk uninstall
	$(MAKE) -C sub/paperwork/paperwork-backend uninstall

linux_exe:
	appimage-builder --skip-tests --recipe AppImageBuilder.yml

windows_exe: version
	rm -rf build/exe
	${MAKE} -C sub/libinsane install PREFIX=/ucrt64
	$(MAKE) -C sub/paperwork/openpaperwork-core install
	$(MAKE) -C sub/paperwork/openpaperwork-gtk install
	$(MAKE) -C sub/paperwork/paperwork-backend install
	${PYTHON} -m pip install .
	${PYTHON} ./setup_cx_freeze.py build_exe
	mv build/exe.mingw* build/exe
	# a bunch of things are missing ...
	cp -Ra /ucrt64/lib/gdk-pixbuf-2.0 build/exe/lib
	mkdir -p build/exe/share
	cp -Ra /ucrt64/share/icons build/exe/share
	cp -Ra /ucrt64/share/locale build/exe/share
	cp -Ra /ucrt64/share/themes build/exe/share
	cp -Ra /ucrt64/share/fontconfig build/exe/share
	makensis pyinstaller/self_extract.nsis
	mkdir -p dist
	mv pyinstaller/ironscanner.exe dist

release:
ifeq (${RELEASE}, )
	@echo "You must specify a release version (make release RELEASE=1.2.3)"
else
	@echo "Will release: ${RELEASE}"
	@echo "Checking release is in ChangeLog ..."
	grep ${RELEASE} ChangeLog
	@echo "Releasing ..."
	git tag -a ${RELEASE} -m ${RELEASE}
	git push origin ${RELEASE}
	@echo "All done"
endif

deps:
	git submodule init
	git submodule update --recursive --remote --init

venv: deps
	echo "Building virtual env"
	make -C sub/libinsane build_c
	virtualenv -p python3 --system-site-packages venv

help:
	@echo "make build"
	@echo "make help: display this message"
	@echo "make install"
	@echo "make uninstall"
	@echo "make exe"

.PHONY: \
	build \
	build_c \
	build_py \
	check \
	clean \
	deps \
	dist-clean \
	doc \
	exe \
	help \
	install \
	install_c \
	install_py \
	release \
	test \
	uninstall \
	uninstall_c \
	venv \
	version \
	windows_exe
