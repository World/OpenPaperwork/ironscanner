import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class _LogHandler(logging.Handler):
    DEFAULT_LOG_FORMAT = '[%(levelname)-6s] [%(name)-4s] %(message)s'

    def __init__(self, plugin):
        super().__init__()
        self.plugin = plugin
        self.log_level = logging.DEBUG
        self.formatter = logging.Formatter(self.DEFAULT_LOG_FORMAT)

        (self.filepath, self.fd) = self.plugin.core.call_success(
            "fs_mktemp", prefix="ironscanner_logs_",
            suffix=".txt", mode="w"
        )
        LOGGER.info("Writing logs to %s", self.filepath)

    def emit(self, record):
        if record.levelno < self.log_level:
            return
        line = self.formatter.format(record)
        line = line.strip()
        self.plugin.core.call_one(
            "mainloop_schedule",
            self.fd.write, line + "\n"
        )

    def flush(self):
        self.fd.flush()


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.log_handler = None

    def get_interfaces(self):
        return [
            'bug_report_attachments',
            'scan_report',
            'uncaught_exception_listener',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'uncaught_exception',
                'defaults': ['openpaperwork_core.uncaught_exception'],
            },
        ]

    def init(self, core):
        super().init(core)
        self.log_handler = _LogHandler(self)
        logging.getLogger().addHandler(self.log_handler)

    def get_scan_report_attachments(self, out_urls: list):
        LOGGER.info("Including logs in the report")
        self.log_handler.flush()
        out_urls.append(("traces.txt", self.log_handler.filepath))
