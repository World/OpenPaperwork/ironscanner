import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.report_success = {}

    def get_interfaces(self):
        return ["scan_report"]

    def get_deps(self):
        return []

    def on_user_scan_test_result(self, successful):
        self.report_success = successful

    def get_scan_report_dict(self, out: dict):
        if 'scantest' not in out:
            out['scantest'] = {}
        LOGGER.info("Report: successful = %s", self.report_success)
        out['scantest']['successful'] = self.report_success
