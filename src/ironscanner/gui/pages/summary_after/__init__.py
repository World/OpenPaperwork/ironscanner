import logging

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps

from .... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -500

    def __init__(self):
        super().__init__()
        self.widget_tree = None

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_assistant_page',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_assistant',
                'defaults': ['ironscanner.gui.mainform'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def init(self, core):
        super().init(core)

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "ironscanner.gui.pages.summary_after", "summary_after.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

    def on_initialized(self):
        page = self.widget_tree.get_object("page_test_results")
        self.core.call_all(
            "assistant_add_page",
            Gtk.AssistantPageType.CONFIRM,
            _("Test done"),
            page
        )
        self.core.call_all("assistant_set_page_complete", page)

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)
