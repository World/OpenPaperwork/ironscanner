import logging

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps

from .... import _


LOGGER = logging.getLogger(__name__)


class _LogHandler(logging.Handler):
    DEFAULT_LOG_FORMAT = '[%(levelname)-6s] [%(name)-4s] %(message)s'

    def __init__(self, plugin):
        super().__init__()
        self.plugin = plugin
        self.log_level = logging.WARNING
        self.formatter = logging.Formatter(self.DEFAULT_LOG_FORMAT)

    def emit(self, record):
        if record.levelno < self.log_level:
            return
        line = self.formatter.format(record)
        line = line.strip()
        self.plugin.core.call_one(
            "mainloop_schedule",
            self.plugin._append_msg, line
        )


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -1000

    def __init__(self):
        super().__init__()
        self.widget_tree = None
        self.msgs = []
        self.log_handler = None

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_assistant_page',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_assistant',
                'defaults': ['ironscanner.gui.mainform'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'scan_report_upload',
                'defaults': ['ironscanner.upload'],
            },
        ]

    def init(self, core):
        super().init(core)

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "ironscanner.gui.pages.upload", "upload.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

        self.log_handler = _LogHandler(self)
        logging.getLogger().addHandler(self.log_handler)

    def on_initialized(self):
        self.core.call_all(
            "assistant_add_page",
            Gtk.AssistantPageType.PROGRESS,
            _("Sending results"),
            self.widget_tree.get_object("page_upload")
        )

    def on_assistant_prepare(self, page):
        if page is not self.widget_tree.get_object("page_upload"):
            return
        self.msgs = []
        self.on_report_upload_progress(0, _("Uploading report ..."))
        self.core.call_all("upload_scan_report")
        self.core.call_all("assistant_commit")

    def on_report_upload_progress(self, progression, msg):
        self.widget_tree.get_object("progressSendingResults").set_fraction(
            progression
        )
        self._append_msg(msg)

    def _append_msg(self, msg):
        self.msgs.append(msg.strip())
        msgs = "\n".join(self.msgs)
        self.widget_tree.get_object("textbufferSendingReport").set_text(msgs)

    def on_report_upload_complete(self, successful, url):
        if successful:
            self.core.call_all("assistant_commit")
            self.on_report_upload_progress(100, _("Upload successful"))
        else:
            self.on_report_upload_progress(100, _("Upload failed"))
        self.core.call_all(
            "assistant_set_page_complete", self.widget_tree.get_object(
                "page_upload"
            )
        )

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)
