import logging

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps

from .... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -1500

    def __init__(self):
        super().__init__()
        self.widget_tree = None

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_assistant_page',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_assistant',
                'defaults': ['ironscanner.gui.mainform'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def init(self, core):
        super().init(core)

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "ironscanner.gui.pages.upload_result", "upload_result.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

    def on_initialized(self):
        self.core.call_all(
            "assistant_add_page",
            Gtk.AssistantPageType.SUMMARY,
            _("Thank you"),
            self.widget_tree.get_object("page_upload_result")
        )

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_report_upload_complete(self, successful, url):
        if successful:
            self.widget_tree.get_object("labelThankYou").set_text(
                _("Thank you")
            )
            self.widget_tree.get_object("labelSent").set_text(
                _(
                    "Test results have been sent to Openpaper.work."
                    " You can access them at the address below:"
                )
            )
            self.widget_tree.get_object("linkbuttonOpenReport").set_uri(url)
            self.widget_tree.get_object("linkbuttonOpenReport").set_label(url)
        else:
            self.widget_tree.get_object("labelThankYou").set_text("")
            self.widget_tree.get_object("labelSent").set_text(
                _("Upload failed. Sorry")
            )
            self.widget_tree.get_object("linkbuttonOpenReport").set_uri("")
            self.widget_tree.get_object("linkbuttonOpenReport").set_label("")
