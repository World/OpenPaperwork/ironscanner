import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.user_info = {}
        self.report = {
            'type': "unknown"
        }

    def get_interfaces(self):
        return ["scan_report"]

    def get_deps(self):
        return []

    def get_info_for_user(self, out: dict):
        out.update(self.user_info)

    def on_scanner_selected(self, devid, dev_name, vendor, model):
        self.user_info['dev_name'] = dev_name
        self.report['vendor'] = vendor
        self.report['model'] = model
        self.report['nicename'] = dev_name
        self.report['devid'] = devid
        self.report['fullname'] = "{} {} ({})".format(
            vendor, model, dev_name
        )
        self.report['sources'] = []
        self.report['options'] = {}

    def on_scanner_info(self, manufacturer, model):
        self.user_info['dev_name'] = "{} {}".format(manufacturer, model)
        self.report['vendor'] = manufacturer
        self.report['model'] = model

    def on_source_list(self, sources):
        self.report['sources'] = sources

    def on_source_options(self, options):
        self.report['options'] = {}
        for (opt_name, opt) in options.items():
            try:
                value = str(opt.get_value())
            except Exception as exc:
                value = "(Exception: {})".format(str(exc))
            self.report['options'][opt_name] = {
                'title': str(opt.get_title()),
                'desc': str(opt.get_desc()),
                'type': str(opt.get_value_type()),
                'unit': str(opt.get_value_unit()),
                'capabilities': str(opt.get_capabilities()),
                'constraint': str(opt.get_constraint()),
                'initial_value': value
            }

    def on_scanner_type_selected(self, scanner_type):
        self.report['type'] = scanner_type
        self.user_info['dev_type'] = scanner_type

    def get_scan_report_dict(self, out: dict):
        LOGGER.info("Report: scanner=%s", self.report)
        out['scanner'] = self.report
