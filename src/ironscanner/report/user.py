import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.user = {}
        self.user_email = ""

    def get_interfaces(self):
        return ['scan_report']

    def get_deps(self):
        return []

    def get_info_for_user(self, out: dict):
        user = {"user_" + k: v for (k, v) in self.user.items()}
        out.update(user)

    def on_user_name(self, name):
        self.user['name'] = name

    def on_user_email(self, email):
        self.user_email = email  # stored separately for privacy reasons

    def on_user_comment(self, comment):
        self.user['comment'] = comment

    def get_scan_report_dict(self, out: dict):
        LOGGER.info("Report: user=%s", self.user)
        out['user'] = self.user

    def get_user_email(self):
        return self.user_email
