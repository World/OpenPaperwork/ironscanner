import logging

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_gtk.deps

from .... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -250

    def __init__(self):
        super().__init__()
        self.widget_tree = None
        self.page_active = False

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_assistant_page',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_assistant',
                'defaults': ['ironscanner.gui.mainform'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def init(self, core):
        super().init(core)

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "ironscanner.gui.pages.scan_result", "scan_result.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

    def on_initialized(self):
        page = self.widget_tree.get_object("page_extras")
        self.core.call_all(
            "assistant_add_page",
            Gtk.AssistantPageType.CONTENT,
            _("Extras"),
            page
        )
        self.core.call_all("assistant_set_page_complete", page)

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_assistant_prepare(self, page):
        if page is not self.widget_tree.get_object("page_extras"):
            if self.page_active:
                self.page_active = False
                self._broadcast_result()
            return
        self.core.call_all("assistant_commit")
        self.page_active = True

    def on_scan_test_result(self, success):
        self.widget_tree.get_object("radiobuttonScanSuccessful").set_active(
            success
        )

    def _broadcast_result(self):
        self.core.call_all(
            "on_user_scan_test_result",
            self.widget_tree.get_object(
                "radiobuttonScanSuccessful"
            ).get_active()
        )

        buf = self.widget_tree.get_object("textbufferUserComment")
        start = buf.get_iter_at_offset(0)
        end = buf.get_iter_at_offset(-1)
        comment = buf.get_text(start, end, False)
        self.core.call_all("on_user_comment", comment)

    def get_scan_report_dict(self, out: dict):
        if 'scantest' not in out:
            out['scantest'] = {}
        out['scantest']['successful'] = self.widget_tree.get_object(
            "radiobuttonScanSuccessful"
        ).get_active()

        if 'user' not in out:
            out['user'] = {}
