import logging

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    GTK_AVAILABLE = True
except (ImportError, ValueError):
    GTK_AVAILABLE = False

import openpaperwork_core
import openpaperwork_core.promise
import openpaperwork_gtk.deps

from .... import _


LOGGER = logging.getLogger(__name__)


class _LogHandler(logging.Handler):
    DEFAULT_LOG_FORMAT = '[%(levelname)-6s] [%(name)-4s] %(message)s'

    def __init__(self, plugin):
        super().__init__()
        self.plugin = plugin
        self.log_level = logging.WARNING
        self.formatter = logging.Formatter(self.DEFAULT_LOG_FORMAT)

    def emit(self, record):
        if record.levelno < self.log_level:
            return
        line = self.formatter.format(record)
        self.plugin.core.call_one(
            "mainloop_schedule",
            self.plugin._display_log_line, line
        )


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 0
    MAX_LOG_LINES = 100

    def __init__(self):
        super().__init__()
        self.widget_tree = None
        self.scan_source = None

        self.page_nb = 0
        self.last_page_progression_notified = 0
        self.page_progression = 0

        self.log_buffer = None
        self.log_lines = []
        self.log_handler = None

        self.img_widget = None

    def get_interfaces(self):
        return [
            'chkdeps',
            'gtk_assistant_page',
            'scan_report',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'gtk_assistant',
                'defaults': ['ironscanner.gui.mainform'],
            },
            {
                'interface': 'gtk_drawer_scan',
                'defaults': ['paperwork_backend.drawer.scan'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
            {
                'interface': 'scan',
                'defaults': ['openpaperwork_core.docscan.libinsane'],
            },
        ]

    def init(self, core):
        super().init(core)

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "ironscanner.gui.pages.scan", "scan.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

        self.log_buffer = self.widget_tree.get_object("textbufferOnTheFly")
        self.log_textview = self.widget_tree.get_object("textviewOnTheFly")
        self.log_vadj = self.widget_tree.get_object(
            "scrolledwindowOnTheFly"
        ).get_vadjustment()

        self.log_handler = _LogHandler(self)
        logging.getLogger().addHandler(self.log_handler)

        self.img_widget = self.widget_tree.get_object("scanDrawing")

    def on_initialized(self):
        self.core.call_all(
            "assistant_add_page",
            Gtk.AssistantPageType.PROGRESS,
            _("Let's scan !"),
            self.widget_tree.get_object("page_scan")
        )

    def chkdeps(self, out: dict):
        if not GTK_AVAILABLE:
            out['gtk'].update(openpaperwork_gtk.deps.GTK)

    def on_source_selected(self, source):
        self.scan_source = source

    def _unwind_scan(self, r):
        (source, scan_id, pages) = r
        last_page = None
        try:
            self.page_nb = 0
            self.page_progression = 0
            LOGGER.info("Scanning page 0 ...")
            self._display_log_line(_("Scanning page %d ...") % 1)
            for page in pages:
                self.core.call_all("on_new_scanned_image", page)

                last_page = page
                self.page_nb += 1
                self.page_progression = 0
                LOGGER.info("Scanning page {} ...".format(self.page_nb))
                self._display_log_line(
                    _("Scanning page %d ...") % (self.page_nb + 1)
                )
            LOGGER.info("End of feed")
            self._display_log_line(_("End of feed"))
            if last_page is not None:
                self.core.call_all(
                    "draw_pillow_start", self.img_widget, last_page
                )
        finally:
            source.close()

    def on_assistant_prepare(self, page):
        if page is not self.widget_tree.get_object("page_scan"):
            return

        if self.scan_source is None:
            # scanner not in the list
            self.core.call_all("assistant_set_page_complete", page)
            self._display_log_line(_("Scanner not found, scan not possible."))
            return

        self.core.call_all("assistant_commit")

        (scan_id, promise) = self.scan_source.scan_promise()

        self.core.call_all("draw_scan_start", self.img_widget, scan_id)

        def purge_logs(args):
            # make things more readable for the user. They don't care about
            # details
            self.log_lines = []
            self._set_log_txt()
            return args

        promise = promise.then(purge_logs)

        promise = promise.then(openpaperwork_core.promise.ThreadedPromise(
            self.core, self._unwind_scan
        ))

        def on_end(success=True):
            self.core.call_all("assistant_set_page_complete", page)
            self.core.call_all("on_scan_test_result", success)
            self.core.call_all("draw_scan_stop", self.img_widget)

        def on_error(exc):
            on_end(False)
            raise exc

        promise = promise.then(on_end)
        promise = promise.catch(on_error)

        self.core.call_success("scan_schedule", promise)

    def _scroll_to_bottom(self, *args, **kwargs):
        upper = self.log_vadj.get_upper()
        self.log_vadj.set_value(upper)

    def _set_log_txt(self):
        txt = "\n".join(self.log_lines)
        self.log_buffer.set_text(txt)
        self._scroll_to_bottom()

    def _display_log_line(self, line):
        line = line.strip()
        self.log_lines.append(line)
        if len(self.log_lines) > self.MAX_LOG_LINES:
            self.log_lines.pop(0)
        self.core.call_one("mainloop_schedule", self._set_log_txt)

    def _set_progress(self, pourcent):
        self.widget_tree.get_object("progressbarTestProgress").set_fraction(
            pourcent / 100
        )

    def on_scan_chunk(self, scan_id, scan_params, pil):
        final_height = scan_params.get_height()
        if final_height == 0:
            # avoid division by 0
            return
        self.page_progression += pil.size[1]
        if self.page_progression > self.last_page_progression_notified + 200:
            pourcent = (self.page_progression * 100 / final_height)
            LOGGER.info("%d%%", pourcent)
            self._display_log_line("%d%%" % pourcent)
            self.last_page_progression_notified = self.page_progression

            self.core.call_one(
                "mainloop_schedule", self._set_progress, pourcent
            )
