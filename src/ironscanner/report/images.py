import logging

import openpaperwork_core

from .. import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    def __init__(self):
        super().__init__()
        self.page_nb = 0
        self.page_images = []

    def get_interfaces(self):
        return ['scan_report']

    def get_deps(self):
        return []

    def on_new_scanned_image(self, img):
        (tmp_filepath, tmp_fd) = self.core.call_success(
            "fs_mktemp", prefix="ironscanner_scan_{}_".format(self.page_nb),
            suffix=".png", mode="wb"
        )
        LOGGER.info(
            "Saving page %d as %s ...", self.page_nb, tmp_filepath
        )
        with tmp_fd:
            img.save(tmp_fd, format="PNG")
        self.page_images.append(tmp_filepath)

        self.page_nb += 1

    def get_scan_report_attachments(self, out_urls: list):
        LOGGER.info("Including %d images in the report", len(self.page_images))
        for (idx, page_url) in enumerate(self.page_images):
            out_urls.append(("page_{:03}.png".format(idx), page_url))

    def bug_report_get_attachments(self, inputs: dict):
        for page in self.page_images:
            inputs[page] = {
                'date': None,  # now
                'include_by_default': True,
                'file_type': _("Scanned page"),
                'file_url': page,
                'file_size': self.core.call_success("fs_getsize", page),
            }
